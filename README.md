# Vereinssatzung

Versioniert die Satzung des Turnverein Niederbrechen 1901 e.V. als LaTeX Dokument.

Die historischen Versionen der Satzung sind als PDF-Dokumente zu finden auf der öffentlichen Seite des Gitlab Projektes ["Vereinssatzung"](https://gitlab.com/tvnprojects/vereinssatzung/-/releases).

Durch Taggen eines Git-Commits wird die Satzung `released`, d.h. aus dem LaTeX-Dokument eine PDF-Datei generiert und über ein Release veröffentlicht.

Das PDF-Dokument wird nach dem Namensschema `Satzung-<Tag>.pdf` benannt.

Das Release erhält den Namen der Tag-Message.

Ein Tag sollte daher in folgendem Format erfolgen.

```
git tag <YYYYMMDD> -m 'Satzung vom DD.MM.YYYY'
```